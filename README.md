# delightful linked data [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of linked data resources for developers.

> **TODO**: Improve layout and structure (see [#1](https://codeberg.org/teaserbot-labs/delightful-linked-data/issues/1)).

## Contents

- [Introduction](#introduction)
- [Specifications](#specifications)
  - [Core specifications](#core-specifications)
  - [Fediverse specifications](#fediverse-specifications)
  - [Solid specifications](#solid-specifications)
  - [Hydra specifications](#hydra-specifications)
- [Information topics](#information-topics)
  - [Linked Data](#linked-data)
  - [JSON-LD](#json-ld)
  - [RDF](#rdf)
  - [Ontologies](#ontologies)
- [Tools](#tools)
- [Libraries](#libraries)
  - [Python](#python)
- [Applications](#applications)
  - [Databases](#databases)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Introduction

After the hype surrounding the [Semantic Web](https://en.wikipedia.org/wiki/Semantic_Web) subsided in 2006 the use of Linked Data remained mostly restricted to scientific circles and for use in [SEO](https://en.wikipedia.org/wiki/Search_engine_optimization), popularized by Google. Much of the power of Linked Data remains untapped.. until 2018 when - with the rise of the ActivityPub-based [Fediverse](https://en.wikipedia.org/wiki/Fediverse) and initiatives such as Tim Berners-Lee [Solid Project](https://solidproject.org/) and [Rebooting The Web of Trust](https://www.weboftrust.info/) - there is renewed interest and growing popularity of Linked Data-based technology.

This curated list, which is part of the [delightful project](https://codeberg.org/teaserbot-labs/delightful), aims to help developers find the best resources available to create new Linked Data applications. The entries in this list are carefully selected from a technology landscape that is still strewn with leftovers from the Semantic Web era that should be avoided. This list offers you the code projects and information resources that are relevant today.

### Please help improve this list

Did you find something that should be part of the list? We'd love :heart: your contribution in the form of an issue on the tracker, or - better yet - a Pull Request to the repo. Contributors can list themselves as [delightful contributors](delightful-contributors.md). Read [here](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#user-content-attribution-of-contributors) how to do that.

> **Important**: Many projects are under active development. To be part of this list your submission should at least have _some_ level of direct applicability and usability / production-readiness. Experimental and alpha-stage codebases are probably not eligible. When in doubt create an issue first, before sending your PR.

## Specifications

### Core specifications

- [W3C Standards and Drafts](https://www.w3.org/TR/) - Overview of all W3C standards and drafts.

W3C specifications that define the Resource Description Framework.

- [W3C RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/) - This primer is designed to provide the reader with the basic knowledge required to effectively use RDF.
- [W3C RDF 1.1 Concepts and Abstract Syntax](https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/) - Representing information in the Web, key concepts and terminology.
- [W3C RDF Schema 1.1](https://www.w3.org/TR/2014/REC-rdf-schema-20140225/) - Data-modelling vocabulary for RDF data, and is an extension of the basic RDF vocabulary.
- [W3C RDF 1.1 Turtle](https://www.w3.org/TR/2014/REC-turtle-20140225/) - Defines a textual syntax for RDF that allows an RDF graph to be written in a compact and natural text form.
- [W3C RDFa Core 1.1](https://www.w3.org/TR/2015/REC-rdfa-core-20150317/) - A specification for attributes to express structured data in any markup language.
- [W3C RDFa Lite 1.1](https://www.w3.org/TR/2015/REC-rdfa-lite-20150317/) - A minimal subset of RDFa, the Resource Description Framework in attributes.
- [W3C HTML+RDFa 1.1](https://www.w3.org/TR/2015/REC-html-rdfa-20150317/) - Rules and guidelines for adapting the RDFa Core 1.1 and RDFa Lite 1.1 for use in (X)HTML5.
- [W3C SPARQL 1.1 Overview](https://www.w3.org/TR/sparql11-overview/) - Facilitate querying and manipulating of RDF graph content on the Web or in an RDF store.

W3C specifications that define OWL or the Web Ontology Language.

- [W3C OWL 2 Overview](https://www.w3.org/TR/owl2-overview/) - An introduction to OWL 2 and the various other OWL 2 documents.
- [W3C OWL 2 Primer](https://www.w3.org/TR/2012/REC-owl2-primer-20121211/) - An approachable introduction to OWL 2.
- [W3C OWL 2 Quick Reference Guide](https://www.w3.org/TR/2012/REC-owl2-quick-reference-20121211/) - A quick reference guide to the OWL 2 language, with links to other documents.
- [W3C OWL 2 Specification](https://www.w3.org/TR/2012/REC-owl2-syntax-20121211/) - An ontology language for the Semantic Web with formally defined meaning.
- [W3C OWL 2 Manchester Syntax](https://www.w3.org/TR/2012/NOTE-owl2-manchester-syntax-20121211/) - A user-friendly compact syntax for OWL 2 ontologies.

W3C specifications that define JSON-LD or JSON-based Linked Data.

- [W3C JSON-LD 1.1](https://www.w3.org/TR/json-ld11/) - A JSON-based format to serialize Linked Data.
- [W3C JSON-LD 1.1 Processing Algorithms and API](https://www.w3.org/TR/json-ld11-api/) - Defines a set of algorithms for programmatic transformations of JSON-LD documents.
- [W3C JSON-LD 1.1 Framing](https://www.w3.org/TR/json-ld11-framing/) - Allows developers to query by example and force a specific tree layout to a JSON-LD document.
- [W3C Streaming JSON-LD](https://www.w3.org/TR/json-ld11-streaming/) - A set of guidelines for efficiently serializing and deserializing JSON-LD in a streaming way.

### Fediverse specifications

- [W3C Activity Streams 2.0](https://www.w3.org/TR/activitystreams-core/) - Details a model for representing potential and completed activities using the JSON format.
- [W3C Activity Vocabulary](https://www.w3.org/TR/activitystreams-vocabulary/) -Provides a foundational vocabulary for activity structures for use with ActivityStreams 2.0.
- [W3C ActivityPub](https://www.w3.org/TR/activitypub/) - A decentralized social networking protocol based upon the ActivityStreams 2.0 data format.

### Solid specifications

> **Note**: As of 2020 the Solid ecosystem specifications are undergoing significant refactoring. The entries below are early drafts and subject to change.

The Solid ecosystem provides a set of specifications that work together.

- [Solid Ecosystem](https://solid.github.io/specification/) - Provide applications with secure and permissioned access to externally stored data in an interoperable way.

The individual Solid specifications are:

- [Web Access Control](https://solid.github.io/specification/wac/) - A mechanism for fine-grained access control to resources on the Web.
- [WebID-OIDC](https://solid.github.io/specification/webid-oidc/) - An authentication mechanism based on the OpenID Connect protocol.
- [WebID-TLS](https://solid.github.io/specification/webid-tls/) - A mechanism for authenticating agents with a WebID through the OpenID Connect protocol.
- [Data Pod Management](https://solid.github.io/specification/pod-management/) - An RDF-driven Web API for managing users and data pods of a Solid pod server.
- [Data Footprints](https://solid.github.io/specification/footprints/) - A description model to decide where new resources are stored and how they are wired up.

### Hydra specifications

Hydra stands for interoperable Hypermedia-Driven Web APIs. The [Hydra working group](http://www.hydra-cg.com/) aims to simplify the development of Hydra APIs and clients.

- [Hydra Core Vocabulary](http://www.hydra-cg.com/spec/latest/core/) - A lightweight vocabulary to create hypermedia-driven Web APIs and enable the creation of generic API clients.
- [Linked Data Fragments](https://linkeddatafragments.org/specification/) - A uniform view on all possible interfaces to publish Linked Data.

## Information topics

### Linked Data

This list contains generic information resources to working with Linked Data.

- [Introduction to LDP](https://hectorcorrea.com/blog/introduction-to-ldp/67) - An introduction to the concept of Linked Data and how to get started with the Linked Data Platform.
- [A data engineer’s guide to semantic modelling](http://blog.thehyve.nl/news/ebook-semantic-model) - Get started with this eBook, or refresh your memory.

### JSON-LD

- [W3C-CG Building JSON-LD APIs: Best Practices](https://json-ld.org/spec/latest/json-ld-api-best-practices/) - Best practices for publishing data using JSON-LD and interacting with such services.
- [Publishing JSON-LD for Developers](https://datalanguage.com/news/publishing-json-ld-for-developers) - A case for publishing JSON-LD regardless whether you are working with linked data or RDF.
- [Modeling Schema.org Schema with TypeScript](https://blog.eyas.sh/2019/05/modeling-schema-org-schema-with-typescript-the-power-and-limitations-of-the-typescript-type-system/) - Showing the power and limitations of the TypeScript type system.
  - [Part 2 - Schema.org Enumerations in TypeScript](https://blog.eyas.sh/2019/05/schema-org-enumerations-in-typescript/)
  - [Part 3 - Schema.org DataType in TypeScript: Structural Typing Doesn't Cut It](https://blog.eyas.sh/2019/07/schema-org-datatype-in-typescript-structural-typing-doesnt-cut-it/)
  - [Part 4 - Schema.org Classes in TypeScript: Properties and Special Cases](https://blog.eyas.sh/2019/07/schema-org-classes-in-typescript-properties-and-special-cases/)

### RDF

[TODO]

### Ontologies

- [ODapps: Ontology-driven apps](https://www.mkbergman.com/2267/combining-knowledge-graphs-and-ontologies-for-dynamic-apps/) - Combining Knowledge Graphs and Ontologies for Dynamic Apps.

## Tools

[TODO]

## Libraries

[TODO]

### Typescript

- [react-schemaorg](https://github.com/google/react-schemaorg) - A component for inserting type-checked Schema.org JSON-LD in your React apps.

### Python

- [pyld](https://github.com/digitalbazaar/pyld) - Python implementation of JSON-LD algorithms. Currently [unmaintained](https://github.com/digitalbazaar/pyld/pull/164#issuecomment-1269997067) and has some problems with default document loading, e.g. for [ActivityStreams](https://github.com/digitalbazaar/pyld/pull/170) and [schema.org](https://github.com/digitalbazaar/pyld/issues/154).
- [rdflib](https://rdflib.readthedocs.io/en/stable/index.html) convert JSON-LD into other formats, e.g. RDF/XML or turtle. Documentation for [parser](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.parsers.html#module-rdflib.plugins.parsers.jsonld) and [serializer](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.serializers.html#module-rdflib.plugins.serializers.jsonld) and [here](https://rdflib.readthedocs.io/en/stable/_modules/rdflib/plugins/serializers/jsonld.html#JsonLDSerializer.serialize) for available options.

## Applications

[TODO]

### Databases

- [TerminusDB](https://github.com/terminusdb/terminusdb-server) - A model driven in-memory graph database designed for the web-age using JSON-LD exchange format.

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-linked-data/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)
